console.log("Hello World!"); //logs a message on the console.
/*
	Syntax:
		console.log(message);

	Statement:
		console.log("Hello World");

*/


//Writing Comments
	//Comments are meant to describe the written code.

//Two Ways of writing comments in JS
/*	
		1.Single Line
			// ctrl + /
		
		2. Multi Line Comment
			(ctrl + shift + /)

*/

/*	
	Variables
		- it is used to contain data.
		- any information that is used by an application is stored in what we call a "memory"
		- when we create variables, certain portion of a device's memory is given a "name" that we call "variables"
	

	Declaring Variables
		- tells our devices that a variable name is created and is ready to store data
		- declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

		Syntax:
			let/const variableName;

	let is a keyword that is usually used in declaring a variable
*/

let myVariable;
console.log(myVariable);
// console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console

//Variables must be declared first before they are used
//console.log(hello); //hello is not defined

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/


/*
	Best Practice in Naming Variables
		1. When naming variables, it is important to create a variables that are descriptive and indicative of the data it contains.

		2.	When naming variables, it is better to start with a lower case. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

		3. Do not add spaces to your variable names. Use camelCase for multiple words.


*/

// let firstName = "Mark Joseph"; //good variable name
// let pokemon = 25000; //bad variable name

// let FamilyName = "Mendoza"; // bad variable name;
// let familyName = "Mendoza"; //good variable name;

// let first name = "Jack";
// let firstName = "Jack";

// lastName, emailAddress, mobileNumber holidayGreeting

//Declaring and Initializing Variables
//Initializing variables - the instance when a variable is given its initial/starting value.

/*
	Syntax:
		let/const variableName = value

*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 59000;
console.log(productPrice);

const pi = 3.1416;
console.log(pi);

//let and const are the keywords that we used to declare a variable
//let - we usually use "let" if we want to reassign the values in our variable

//Reassigning variable values
//Reassigning a variable means changing it's initial or previous value into another value
/*
	Syntax:
		variableName = newValue

*/

productName = "Laptop";
console.log(productName);

let bootcamper = "Lexus";
bootcamper = "Jayson";
console.log(bootcamper);

// let student = "Lexus";
// let student = "Jayson"; //Identifier 'student' has already been declared
// console.log(student);


//const cannot be updated or re-declared
//Values of constants cannot be changed and will simply return an error
// pi = 555; // Assignment to constant variable.
// console.log(pi);

/*

	When to use use JS const?
		As a general rule, always declare a variable with const unless you know that the value will change.


	Use const when you declare:
		- A new Array
		- A new Object
		- A new Function
*/

//Reassigning variables vs initializing variables
//Declares a variable first
let supplier;
//Initialization is done after the variable has been declared;
console.log(supplier);
//This is considered as initialization because it is the first time that a value has been assigned to a variable;
supplier = "John Smith Tradings";
//let supplier = "John Smith Tradings";
console.log(supplier);
//This is considered as reassignment because its initial value was already declared
supplier = "Zuitt Store";
console.log(supplier);

//Can you declare a const variable without initialization? No. An error will occur

// const interestRate;
// interestRate = 5.15;
// console.log(interestRate);

//Const variables are variables with constant data. Therefore, we should not re-declare, re-assign, or even declare a const variable without initialization.


//var vs. let/const

//var - is also used in declaring a variable, but var is an ECMAScript1(ES1) feature

//let/const was introduced as a new feature in ES6 updates

//What makes let/const different from var?

//There are issues associated with variables declared with var, regarding hoisting.
//Hoisting is JavaScript's default behavior of moving declarations to the top.
//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.

//example;
	a = 5;
	console.log(a);
	var a;

	// b = 5;
	// console.log(b);
	// let b;

//let/const scope
//Scope essentially means where these variables are available for use
//let and const are block scope
//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.
//So a variable declared in a block with let  is only available for use within that block. 

let outerVariable = "Hello";

{
	let innerVariable = "Hello Again";
	console.log(innerVariable);

}

console.log(outerVariable);

//var - global scope
var outerVariable1 = "Hello - Var";

{
	var innerVariable1 = "Hello Again - Var";
}

console.log(outerVariable1);
console.log(innerVariable1);


//Multiple variable declarations
// Multiple variables may be declared in one line
// Though it is quicker to do without having to retype the "let" keyword, it is still best practice to use multiple "let"/"const" keywords when declaring variables
// Using multiple keywords makes code easier to read and determine what kind of variable has been created

//const productCode = 'DC017', productBrand = 'Dell';
 let productCode = 'DC017';
 const productBrand = 'Dell';
console.log(productCode, productBrand);


//We cannot use a reserved keyword as a variable name
// const let = "Hello";
// console.log(let);

/*

	DATA TYPES

	1. Strings
		- are a series of characters that create a word, phrase, a sentence or anything related to creating a text
		- can be written either a single quote (') or using a double quote ()


*/

let country = 'Philippines';
let province = "Metro Manila";
console.log(country);
console.log(province);

//Concatenate strings
let fullAddress = province + ', ' + country
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

//The escape character(\) in strings in combination with other characters can produce different effects

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early.';
console.log(message);

message = `John's employees went home "early"`;
console.log(message);

//NUMBERS
// Integers/Whole (Negative/Positive)
let headCount = 20;
console.log(headCount);

let numberOfBooks = -1;
console.log(numberOfBooks);

// Decimal/Fraction
let grade = 98.7
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining numbers and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to a certain scenario

let isMarried = false;
let isSingle = true;

console.log("Is 25 years old is the marrying age in the Philippines? " + isMarried);
console.log("Is Chiz Escudero not married? " + isMarried);
console.log("Is Liza Soberano single? " + isSingle);

// Arrays
// Arrays are special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types
/*
	syntax:
		let/const arrayName = [elementA, elementB,.....]
*/

let basketOfFruits = ["bananas", "apple", "mango", "strawberry"];
console.log(basketOfFruits);

// similar data types
let grades = [98.7, 77.3, 90.8];
console.log(grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense to the context of programming
let random = ["Jungkook", 24, true];
console.log(random);

// Objects
// Objects are another special kind of data type that's used to mimic real world object/item
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called property of the object
/*
	syntax:
		lex/const objectName = {
			propertyA: valueA,
			propertyB: valueB,
		}

*/
let person = {
	fullName: "Edward Scissorhand",
	age: 35,
	isMarried: false,
	contactNumbers: ["0915879741", "09065874587"],
	address: {
		houseNumber: "345",
		village: "Futura Homes",
		city: "Manila",
	}
}
console.log(person);

// They're also useful for creating abstract objects
const myGrades = {
	firstGrading: 98.7,
	secondGrading: 77.3,
	thirdGrading: 88.6,
	fourthGrading: 74.9,
}
console.log(myGrades);

// typeof operator
// typeof operator is used to determine the type of data or the value of a variable
console.log(typeof myGrades);//object
console.log(typeof grades);//result: object (special type of object)
// note: array is a special type of object with methods and function to manipulate it -s22 : JavaScript Array Manipulation

// for example
// const anime = ["One Piece", "One Punch Man", "Attack on Titan"];
// anime = ["Kimetsu no Yaiba"];
// console.log(anime);//Assignment to constant variable

// but if we do this:
const anime = ["One Piece", "One Punch Man", "Attack on Titan"];
console.log(anime[0]);
anime [0] = ["Kimetsu no Yaiba"];
console.log(anime);
// we can change the element of an array assigned to a constant variable
// we can change the object's properties assigned to a constant variable

/*
	Constant Object and Array
		the keyword const is a little misleading

		it does not define a constant value, it defines a constant reference to a value

		because of this you can NOT:
		reassign a constant value
		reassign a constant array
		reassign a constant object

		but you CAN:
		change the element of constant array
		change the properties of constant object
*/

// Null
// it is used to intentionally express the absence of a value in a variable declaration/initialization

let spouse = null;
console.log(spouse);
// using null compared to a 0 value and an empty string is much better for readibility
// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string

let myNumber = 0;
let myString = ' ';

// Undefined
// represents the state of a variable that has been declared but without any assigned value

let sssNumber;
console.log(sssNumber);

/* packages in sublime
	ctrl + shirt + p
	- install package
		-browser sync
		-emmet
		-material theme 
*/